'''
 * Copyright (c) 2014, 2015 Entertainment Intelligence Lab, Georgia Institute of Technology.
 * Originally developed by Mark Riedl.
 * Last edited by Mark Riedl 05/2015
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
'''

import sys, pygame, math, numpy, random, time, copy
from pygame.locals import * 

from constants import *
from utils import *
from core import *
from moba import *

class MyMinion(Minion):

    def __init__(self, position, orientation, world, image = NPC, speed = SPEED, viewangle = 360, hitpoints = HITPOINTS, firerate = FIRERATE, bulletclass = SmallBullet):
        Minion.__init__(self, position, orientation, world, image, speed, viewangle, hitpoints, firerate, bulletclass)
        self.states = [Idle]
        ### Add your states to self.states (but don't remove Idle)
        ### YOUR CODE GOES BELOW HERE ###
        self.states.append(Move)
        self.states.append(AttackMini)
        self.states.append(AttackHero)
        self.states.append(AttackTower)
        self.states.append(AttackBase)
        ### YOUR CODE GOES ABOVE HERE ###

    def start(self):
        Minion.start(self)
        self.changeState(Idle)





############################
### Idle
###
### This is the default state of MyMinion. The main purpose of the Idle state is to figure out what state to change to and do that immediately.

class Idle(State):

    def enter(self, oldstate):
        State.enter(self, oldstate)
        # stop moving
        self.agent.stopMoving()

    def execute(self, delta = 0):
        State.execute(self, delta)
        ### YOUR CODE GOES BELOW HERE ###
        visibleList = self.agent.getVisible()
        NearbyBase = None
        NearbyTower = None
        NearbyHero = None
        NearbyMini = None

        enemyBase = self.agent.world.getEnemyBases(self.agent.team)
        enemyTower = self.agent.world.getEnemyTowers(self.agent.team)


        if visibleList != None and len(visibleList) > 0:
            for visibleItem in visibleList:
                if isinstance(visibleItem, Bullet):
                    continue
                if visibleItem.team == 0:
                    continue
                if visibleItem.team == self.agent.team:
                    if isinstance(visibleItem, MOBAAgent) and distance(visibleItem.position, self.agent.position) <= 5:
                        temp = random.randint(0,len(visibleList) - 1)
                        tempItem = visibleList[temp]
                        self.agent.changeState(Move, tempItem)
                        return
                    continue
                if distance(self.agent.position, visibleItem.position) > SMALLBULLETRANGE:
                    continue
                if isinstance(visibleItem, Tower):
                    NearbyTower = visibleItem
                elif isinstance(visibleItem, Base):
                    NearbyBase = visibleItem
                elif isinstance(visibleItem, Minion):
                    NearbyMini = visibleItem
                elif isinstance(visibleItem, Hero):
                    NearbyHero = visibleItem

            if NearbyTower != None:
                self.agent.changeState(AttackTower, NearbyTower)
                return
            elif NearbyBase != None:
                self.agent.changeState(AttackBase, NearbyBase)
                return
            elif NearbyMini != None:
                self.agent.changeState(AttackMini, NearbyMini)
                return
            elif NearbyHero != None:
                self.agent.changeState(AttackHero, NearbyHero)
                return


        if len(enemyTower) > 0 and enemyTower[0].alive:
            self.agent.changeState(Move, enemyTower[0])
            return
        if len(enemyTower) > 1 and enemyTower[1].alive:
            self.agent.changeState(Move, enemyTower[1])
            return
        if len(enemyBase) > 0:
            self.agent.changeState(Move, enemyBase[0])

        ### YOUR CODE GOES ABOVE HERE ###
        return None

##############################
### Taunt
###
### This is a state given as an example of how to pass arbitrary parameters into a State.
### To taunt someome, Agent.changeState(Taunt, enemyagent)

class Taunt(State):

    def parseArgs(self, args):
        self.victim = args[0]

    def execute(self, delta = 0):
        if self.victim is not None:
            print "Hey " + str(self.victim) + ", I don't like you!"
        self.agent.changeState(Idle)

##############################
### YOUR STATES GO HERE:

class Move(State):

    def parseArgs(self, args):
        self.target = args[0]

    def enter(self, oldstate):
        State.enter(self, oldstate)
        self.agent.navigateTo(self.target.position)
        self.countdown = 2000

    def execute(self, delta = 0):
        State.execute(self, delta)
        self.countdown -= delta
        if isinstance(self.target, Tower):
            dist = distance(self.agent.position, self.target.position)
            if dist <= TOWERBULLETRANGE and dist > SMALLBULLETRANGE:
                self.countdown = -1
                return
        if isinstance(self.target, Base):
            dist = distance(self.agent.position, self.target.position)
            if dist <= BASEBULLETRANGE and dist > SMALLBULLETRANGE:
                self.countdown = -1
                return
        if self.countdown <= 0:
            self.agent.changeState(Idle)
        return

class AttackMini(State):

    def parseArgs(self, args):
        self.target = args[0]

    def enter(self, oldstate):
        State.enter(self, oldstate)
        self.countdown = 2000
        self.agent.stopMoving()

    def execute(self, delta = 0):
        self.agent.turnToFace(self.target.position)
        self.agent.shoot()
        self.countdown -= delta
        if not self.target.alive:
            self.agent.changeState(Idle)
        if self.countdown <= 0:
             self.agent.changeState(Idle)

class AttackHero(State):

    def parseArgs(self, args):
        self.target = args[0]

    def enter(self, oldstate):
        State.enter(self, oldstate)
        self.countdown = 2000
        self.agent.stopMoving()

    def execute(self, delta = 0):
        self.agent.turnToFace(self.target.position)
        self.agent.shoot()
        self.countdown -= delta
        if not self.target.alive:
            self.agent.changeState(Idle)
        if self.countdown <= 0:
            self.agent.changeState(Idle)

class AttackTower(State):

    def parseArgs(self, args):
        self.target = args[0]

    def enter(self, oldstate):
        State.enter(self, oldstate)
        self.countdown = 2000
        self.agent.stopMoving()

    def execute(self, delta = 0):
        self.agent.turnToFace(self.target.position)
        self.agent.shoot()
        self.countdown -= delta
        if not self.target.alive:
            self.agent.changeState(Idle)
        if self.countdown <= 0:
            self.agent.changeState(Idle)

class AttackBase(State):

    def parseArgs(self, args):
        self.target = args[0]

    def enter(self, oldstate):
        State.enter(self, oldstate)
        self.countdown = 2000
        self.agent.stopMoving()

    def execute(self, delta = 0):
        self.agent.turnToFace(self.target.position)
        self.agent.shoot()
        self.countdown -= delta
        if not self.target.alive:
            self.agent.changeState(Idle)
        if self.countdown <= 0:
            self.agent.changeState(Idle)
